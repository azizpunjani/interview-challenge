import Class, { defaultGrader } from '../class';

function Student(id, firstName, lastName) {
  this.id = id;
  this.firstName = firstName;
  this.lastName = lastName;
}

const Teacher = () => { };

describe('Class', () => {
  test('properties passed in via constructor are set as expected', () => {
    const grader = jest.fn();
    const teacher = new Teacher();
    const mathClass = new Class(1, 'math', teacher, grader);

    expect(mathClass.id).toBe(1);
    expect(mathClass.description).toBe('math');
    expect(mathClass.teacher).toBe(teacher);
    expect(grader).toBe(grader);
  });

  test('properties not passed in via constructor are initialized as expected', () => {
    const teacher = new Teacher();
    const mathClass = new Class(1, 'math', teacher);

    expect(mathClass.students).toEqual({});
    expect(mathClass.grader).toBe(defaultGrader);
  });

  test('addStudent adds a student to the internal students object', () => {
    const teacher = new Teacher();
    const mathClass = new Class(1, 'math', teacher);
    const student = new Student(3, 'Bob', 'Howard');

    mathClass.addStudent(student);
    expect(mathClass.students[3]).toBe(student)
  });

  test('getQuizScores returns the correct scores', () => {
    const teacher = new Teacher();
    const mathClass = new Class(1, 'math', teacher);
    
    const studentOneScores = [
      { id: 11, description: 'quiz 1', score: 50 },
      { id: 22, description: 'quiz 2', score: 60 }
    ];

    const studentTwoScores = [
      { id: 11, description: 'quiz 1', score: 10 },
      { id: 22, description: 'quiz 2', score: 10 }
    ];

    const studentOne = new Student(111, 'Tom', 'Hanks');
    const studentTwo = new Student(222, 'Meg', 'Ryan');

    studentOne.getQuizzesForClass = () => studentOneScores;
    studentTwo.getQuizzesForClass = () => studentTwoScores;

    mathClass.addStudent(studentOne);
    mathClass.addStudent(studentTwo);

    expect(mathClass.getQuizScores()).toEqual([
      {
        id: 111,
        firstName: 'Tom',
        lastName: 'Hanks',
        quizScores: studentOneScores
      },
      {
        id: 222,
        firstName: 'Meg',
        lastName: 'Ryan',
        quizScores: studentTwoScores
      }
    ]);
  });

  describe('getSemesterGrades', () => {
    const grader = jest.fn().mockImplementation(() => 'TEST GRADE');
    const teacher = new Teacher();
    const mathClass = new Class(1, 'math', teacher, grader);

    const studentOneScores = [
      { id: 11, description: 'quiz 1', score: 50 },
      { id: 22, description: 'quiz 2', score: 60 }
    ];

    const studentTwoScores = [
      { id: 11, description: 'quiz 1', score: 10 },
      { id: 22, description: 'quiz 2', score: 10 }
    ];

    const studentOne = new Student(111, 'Tom', 'Hanks');
    const studentTwo = new Student(222, 'Meg', 'Ryan');

    studentOne.getQuizzesForClass = () => studentOneScores;
    studentTwo.getQuizzesForClass = () => studentTwoScores;

    mathClass.addStudent(studentOne);
    mathClass.addStudent(studentTwo);

    const results = mathClass.getSemesterGrades();

    test('returns the correct average scores', () => {
      expect(results[0].averageScore).toBe(55);
      expect(results[1].averageScore).toBe(10);
    });

    test('expect quizScores to be accurate', () => {
      expect(results[0].quizScores).toEqual(studentOneScores);
      expect(results[1].quizScores).toEqual(studentTwoScores);
    });

    test('expect student first and last names to be accurate', () => {
      expect(results[0].studentFirstName).toBe('Tom');
      expect(results[0].studentLastName).toBe('Hanks');

      expect(results[1].studentFirstName).toBe('Meg');
      expect(results[1].studentLastName).toBe('Ryan');
    });

    test('graderFn was called with the correct arguments', () => {
      expect(grader).toHaveBeenCalledTimes(2);
      expect(grader).toHaveBeenCalledWith(55);
      expect(grader).toHaveBeenCalledWith(10);
    });

    test('expect grade to be what was retuned by graderFn', () => {
      expect(results[0].grade).toBe('TEST GRADE');
      expect(results[1].grade).toBe('TEST GRADE');
    });
  });

  describe('defaultGrader', () => {
    [
      { value: 85, grade: 'A' },
      { value: 84, grade: 'B' },
      { value: 75, grade: 'B' },
      { value: 74, grade: 'C' },
      { value: 65, grade: 'C' },
      { value: 64, grade: 'D' },
      { value: 55, grade: 'D' },
      { value: 54, grade: 'E' },
      { value: 45, grade: 'E' },
      { value: 44, grade: 'F' },
      { value: 20, grade: 'F' },
      { value: 0, grade: 'F' }
    ].forEach(data => {
      test(`should return ${data.grade} for ${data.value}`, () => {
        expect(defaultGrader(data.value)).toBe(data.grade);
      });
    })
  });
});
