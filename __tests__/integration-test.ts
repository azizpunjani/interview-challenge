import Class from '../class';
import Student from '../student';
import Teacher from '../teacher';
import Quiz from '../quiz';
import Question from '../question';
import { each } from 'lodash';

describe('Integration Test Class, Student, Teacher, Quiz and Question', () => {
  const teacher = new Teacher(1, 'Fred', 'Wilson');
  const mathClass = new Class(2, 'Math 101', teacher);
  const student1 = new Student(3, 'Mary', 'Johnson');
  const student2 = new Student(4, 'Michael', 'Jackson');

  mathClass.addStudent(student1);
  mathClass.addStudent(student2);
  teacher.addClass(mathClass);

  const quiz1Data = {
    id: 5,
    description: 'quiz 1',
    questions: [
      {
        id: 6,
        question: 'What is 1 + 1',
        choices: {
          a: 2,
          b: 3,
          c: 4,
          d: 5
        },
        correctAnswer: 'a'
      },
      {
        id: 7,
        question: 'What is 2 + 2',
        choices: {
          a: 2,
          b: 3,
          c: 4,
          d: 5
        },
        correctAnswer: 'c'
      }
    ]
  };

  const quiz2Data = {
    id: 8,
    description: 'quiz 2',
    questions: [
      {
        id: 9,
        question: 'What is 4 + 4',
        choices: {
          a: 8,
          b: 4,
          c: 6,
          d: 12
        },
        correctAnswer: 'a'
      },
      {
        id: 10,
        question: 'What is 2 * 2',
        choices: {
          a: 8,
          b: 4,
          c: 6,
          d: 12
        },
        correctAnswer: 'b'
      }
    ]
  };

  const createQuiz = (classId, { id, description, questions }) => {
    const quiz = new Quiz(id, classId, description);
    
        questions.forEach((q) =>
          quiz.addQuestion(new Question(q.id, q.choices, q.correctAnswer))
        );
    
        return quiz;
  }

  each(teacher.classes[mathClass.id].students, (student) => {
    teacher.assignQuizToStudentInClass(
        mathClass.id, 
        student.id, 
        createQuiz(mathClass.id, quiz1Data)
    );
    
    teacher.assignQuizToStudentInClass(
      mathClass.id, 
      student.id, 
      createQuiz(mathClass.id, quiz2Data)
    );
  });  

  student1.answerQuizQuestion(quiz1Data.id, quiz1Data.questions[0].id, 'a');
  student1.answerQuizQuestion(quiz1Data.id, quiz1Data.questions[1].id, 'c');
  student1.answerQuizQuestion(quiz2Data.id, quiz2Data.questions[0].id, 'a');
  student1.answerQuizQuestion(quiz2Data.id, quiz2Data.questions[1].id, 'b');

  student2.answerQuizQuestion(quiz1Data.id, quiz1Data.questions[0].id, 'a');
  student2.answerQuizQuestion(quiz1Data.id, quiz1Data.questions[1].id, 'c');

  describe('End of semester grades', () => {
    const results = teacher.getGradesForClass(mathClass.id);
    const studentOne = results[0];
    const studentTwo = results[1];

    test('average scores are accurate', () => {
      expect(studentOne.averageScore).toEqual(100);
      expect(studentTwo.averageScore).toEqual(50);
    });

    test('student first and last names to be accurate', () => {
      expect(studentOne.studentFirstName).toBe('Mary');
      expect(studentOne.studentLastName).toBe('Johnson');

      expect(studentTwo.studentFirstName).toBe('Michael');
      expect(studentTwo.studentLastName).toBe('Jackson');
    });

    test('quiz scores should be accurate', () => {
      expect(studentOne.quizScores[0]).toEqual({
        description: 'quiz 1',
        id: 5,
        score: 100
      });

      expect(studentOne.quizScores[1]).toEqual({
        description: 'quiz 2',
        id: 8,
        score: 100
      });

      expect(studentTwo.quizScores[0]).toEqual({
        description: 'quiz 1',
        id: 5,
        score: 100
      });

      expect(studentTwo.quizScores[1]).toEqual({
        description: 'quiz 2',
        id: 8,
        score: 0
      });
    });
    
    test('grades are accurate', () => {
      expect(studentOne.grade).toEqual('A');
      expect(studentTwo.grade).toEqual('E');
    });
  });
});
