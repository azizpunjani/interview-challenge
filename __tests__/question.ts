import Question from '../question';

describe('Question', () => {
  it('properties passed in via constructor are set as expected', () => {
    const choices = {
      a: 'first choice'
    };
    const question = new Question(1, choices, 'a');

    expect(question.id).toBe(1);
    expect(question.choices).toBe(choices);
    expect(question.correctAnswer).toBe('a');
  });
});
