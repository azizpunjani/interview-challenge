import Quiz from '../quiz';

let quiz: Quiz;

function Question(id, correctAnswer?) {
  this.id = id;
  this.correctAnswer = correctAnswer;
}

beforeEach(() => {
  quiz = new Quiz(1, 223, 'biology 101');
})

describe('Quiz', () => {
  test('properties passed in via constructor are set as expected', () => {
    expect(quiz.id).toBe(1);
    expect(quiz.classId).toBe(223);
    expect(quiz.description).toBe('biology 101');
  });

  test('properties not passed in via constructor are intestialized as expected', () => {
    expect(quiz.questions).toEqual({});
    expect(quiz.answers).toEqual({});
  });

  test('addQuestion adds a question to the internal questions array', () => {
    const question = new Question(133, 'a');
    quiz.addQuestion(question);

    expect(quiz.questions[133]).toBe(question);
  });

  test('answerQuestion adds an answer to the internal questions array', () => {
    quiz.addQuestion(new Question(333));
    quiz.answerQuestion(333, 'C');

    expect(quiz.answers[333]).toBe('C');
  });

  test('answerQuestion throws an error when question does not exist', () => {
    expect(() => {
      quiz.answerQuestion(333, 'C');
    }).toThrow("Cannot answer question with id 333, question doesn't exist in quiz");
  });

  describe('score', () => {
    test('score of 0 is returned by default', () => {
      expect(quiz.score).toBe(0);
    });

    test('computes the expected score', () => {
      const question1 = new Question(1, 'a');
      const question2 = new Question(2, 'b');

      quiz.addQuestion(question1);
      quiz.addQuestion(question2);

      quiz.answerQuestion(2, 'b');

      expect(quiz.score).toBe(50);
    });
  });


});
