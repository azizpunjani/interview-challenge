import Student from '../student';

let student: Student;

beforeEach(() => {
  student = new Student(1, 'Bob', 'Howard');
});

function Quiz(id, classId?) {
  this.id = id;
  this.classId = classId;
}

test('properties passed in via constructor are set as expected', () => {
  expect(student.id).toBe(1);
  expect(student.firstName).toBe('Bob');
  expect(student.lastName).toBe('Howard');
});

test('properties not passed in via constructor are initialized as expected', () => {
  expect(student.quizzes).toEqual({});
});

test('assignQuiz assigns a quiz to the internal quizzes property', () => {
  const quiz = new Quiz(123);
  student.assignQuiz(quiz);

  expect(student.quizzes[123]).toBe(quiz);
});

test('answerQuizQuestion calls answerQuestion method on quiz object', () => {
  const quiz = new Quiz(123);
  quiz.answerQuestion = jest.fn();

  student.assignQuiz(quiz);
  student.answerQuizQuestion(123, 1, 'a');

  expect(quiz.answerQuestion).toHaveBeenCalledWith(1, 'a');
});

test('getQuizzesForClass returns the expected quizzes', () => {
  const quiz1 = new Quiz(1, 333);
  const quiz2 = new Quiz(2, 333);

  student.assignQuiz(quiz1);
  student.assignQuiz(quiz2);

  expect(student.getQuizzesForClass(333)).toEqual([ quiz1, quiz2 ]);
});

test('answerQuizQuestion throws when question does not exist in quiz', () => {
  const quiz = new Quiz(44);
  student.assignQuiz(quiz);

  expect(() =>{
    student.answerQuizQuestion(123, 1, 'a');
  }).toThrow('Cannot answer quiz question with id 1, because quiz with id 123 does not exist');
});
