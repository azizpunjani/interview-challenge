import Teacher from '../teacher';

const Quiz = () => { };

function Class(id, students?) {
  this.id = id;
  this.students = students;
}

let teacher: Teacher;

beforeEach(() => {
  teacher = new Teacher(1, 'Fred', 'Wilson');
});

describe('Teacher', () => {
  test('properties passed in via constructor are set as expected', () => {
    expect(teacher.id).toBe(1);
    expect(teacher.firstName).toBe('Fred');
    expect(teacher.lastName).toBe('Wilson');
  });

  test('properties not passed in via constructor are initialized as expected', () => {
    expect(teacher.classes).toEqual({});
  });

  test('addClass adds a class to the internal classes object', () => {
    const mathClass = new Class(1);
    teacher.addClass(mathClass);

    expect(teacher.classes[1]).toBe(mathClass);
  });

  test('assignQuizToStudentInClass calls assignQuiz on the student object', () => {
    const assignQuiz1 = jest.fn();
    const assignQuiz2 = jest.fn();
    const quiz = new Quiz();
    const students = {
      2: { assignQuiz: assignQuiz1 },
      3: { assignQuiz: assignQuiz2 }
    };

    const mathClass = new Class(1, students);
    teacher.addClass(mathClass);

    teacher.assignQuizToStudentInClass(1, 2, quiz);
    expect(assignQuiz1).toBeCalledWith(quiz);

    teacher.assignQuizToStudentInClass(1, 3, quiz);
    expect(assignQuiz2).toBeCalledWith(quiz);
  });

  test('assignQuizToStudentInClass throws an error if teacher does not teach class', () => {
    expect(() => {
      teacher.assignQuizToStudentInClass(1, 2, new Quiz());
    }).toThrow(
      'Teacher with id 1 does not teach class with id 1'
    );
  });

  test('assignQuizToStudentInClass throws an error if student is not in class', () => {
    const assignQuiz = jest.fn();
    const students = {};

    const mathClass = new Class(1, students);
    teacher.addClass(mathClass);

    expect(() => {
      teacher.assignQuizToStudentInClass(1, 999, new Quiz());
    }).toThrow(
      'Could not find student with id 999 in class with id 1'
    );
  });
});
