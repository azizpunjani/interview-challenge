import { map } from 'lodash';
import Student from './student';
import Teacher from './teacher';

export const defaultGrader = (value): string => {
  if (value >= 85) return 'A';
  else if (value >= 75 && value < 85) return 'B';
  else if (value >= 65 && value < 75) return 'C';
  else if (value >= 55 && value < 65) return 'D';
  else if (value >= 45 && value < 55) return 'E';
  else if (value < 45) return 'F';
};

interface Score {
  id: number,
  description: string,
  score: number
}

interface QuizScores {
  id: number;
  quizScores: Array<Score>,
  firstName: string,
  lastName: string
}

interface SemesterGrade {
  id: number;
  quizScores: Array<Score>;
  studentFirstName: string;
  studentLastName: string;
  averageScore: number;
  grade: string;
}

export default class Class {
  readonly id: number;
  readonly description: string;
  readonly teacher: Teacher;
  readonly grader: Function;
  readonly students: { [s: number]: Student };

  constructor(id: number, description: string, teacher: Teacher, grader = defaultGrader) {
    this.id = id;
    this.description = description;
    this.teacher = teacher;
    this.grader = grader;
    this.students = {};
  }

  addStudent(student: Student): void {
    this.students[student.id] = student;
  }

  getQuizScores(): Array<QuizScores> {
    return map(this.students, (student: Student) => {
      const quizScores = student
        .getQuizzesForClass(this.id)
        .map((quiz) => {
          return {
            id: quiz.id,
            description: quiz.description,
            score: quiz.score
          }
        });

      return {
        id: student.id,
        quizScores: quizScores,
        firstName: student.firstName,
        lastName: student.lastName
      }
    });
  }
  
  getSemesterGrades(): Array<SemesterGrade> {
    return this.getQuizScores().map((student) => {
      const totalScore = student
        .quizScores
        .map((quiz) => quiz.score)
        .reduce((acc, curr) => acc + curr, 0);

      const averageScore = totalScore / student.quizScores.length;
      const grade = this.grader(averageScore);

      return {
        id: student.id,
        quizScores: student.quizScores,
        studentFirstName: student.firstName,
        studentLastName: student.lastName,
        averageScore,
        grade
      };
    });
  }
}
