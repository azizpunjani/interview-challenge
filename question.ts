
export default class Question {
  readonly id: number;
  readonly choices: { [s: string]: string };
  readonly correctAnswer: string;

  constructor(id: number, choices: { [s: string]: string }, correctAnswer: string) {
    this.id = id;
    this.choices = choices;
    this.correctAnswer = correctAnswer;
  }
}
