import { filter } from 'lodash';
import Question from './question';

export default class Quiz {
  readonly id: number;
  readonly description: string;
  readonly classId: number;
  readonly questions: { [s: number]: Question };
  readonly answers: { [s: number]: string };

  constructor(id: number, classId: number, description: string) {
    this.id = id;
    this.description = description;
    this.classId = classId;
    this.questions = {};
    this.answers = {};
  }

  addQuestion(question: Question): void {
    this.questions[question.id] = question;
  }

  answerQuestion(questionId: number, studentAnswer: string): void {
    if (!this.questions[questionId]) {
      throw new Error(`Cannot answer question with id ${questionId}, question doesn't exist in quiz`);
    }
    
    this.answers[questionId] = studentAnswer;
  }

  get score(): number {
    const correctAnswers = filter(
      this.answers,
      (studentAnswer, id) => this.questions[id].correctAnswer === studentAnswer
    ).length;

    const numberOfQuestions = Object.keys(this.questions).length;
    const score = correctAnswers === 0
      ? 0
      : (correctAnswers / numberOfQuestions) * 100;

    return score;
  }
}
