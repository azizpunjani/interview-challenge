import Quiz from './quiz';
import { filter } from 'lodash';

export default class Student {
  readonly id: number;
  readonly firstName: string;
  readonly lastName: string;
  readonly quizzes: { [s: number]: Quiz };

  constructor(id: number, firstName: string, lastName: string) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.quizzes = {};
  }

  assignQuiz(quiz: Quiz): void {
    this.quizzes[quiz.id] = quiz;
  }

  answerQuizQuestion(quizId: number, questionId: number, choice: string): void {
    if (!this.quizzes[quizId]) {
      throw new Error(`Cannot answer quiz question with id ${questionId}, because quiz with id ${quizId} does not exist`);
    }

    this.quizzes[quizId].answerQuestion(questionId, choice);
  }

  getQuizzesForClass(classId: number): Array<Quiz> {
    return filter(this.quizzes, (quiz) => quiz.classId === classId);
  }
}
