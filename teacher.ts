import Quiz from './quiz';
import Class from './class';

export default class Teacher {
  readonly id: number;
  readonly firstName: string;
  readonly lastName: string;
  readonly classes: { [s: number]: Class };

  constructor(id: number, firstName: string, lastName: string) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.classes = {};
  }

  addClass(classTaught: Class): void {
    this.classes[classTaught.id] = classTaught;
  }

  assignQuizToStudentInClass(classId: number, studentId: number, quiz: Quiz): void {
    if (!this.classes[classId]) {
      throw new Error(`Teacher with id ${this.id} does not teach class with id ${classId}`);
    }

    if (!this.classes[classId].students[studentId]) {
      throw new Error(`Could not find student with id ${studentId} in class with id ${classId}`);
    }

    this.classes[classId]
      .students[studentId]
      .assignQuiz(quiz);
  }

  getGradesForClass(classId: number) {
    return this.classes[classId].getSemesterGrades();
  }
}
